class CreateMoneyBirdConfigs < ActiveRecord::Migration[6.1]
  def change
    create_table :money_bird_configs do |t|
      t.text :access_token
      t.text :refresh_token
      t.string :token_type
      t.string :created_on_mb
      t.string :record_for
      t.timestamps
    end

    #This can be populated with Active Admin as well
    MoneyBirdConfig.find_or_create_by(access_token: "xByu2Mi2qzE2jGNMyM6-PccNIU93N9FEACnoc6sor80", refresh_token: "c4LeHTdSDeurwsXO_Czu1Jx_ovy3PrMuESJQQhKBLA8", token_type: "Bearer", created_on_mb: "1657538368", record_for: "Authorization")
  end
end
