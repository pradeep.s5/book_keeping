class CreateInvoices < ActiveRecord::Migration[6.1]
  def change
    create_table :invoices do |t|
      t.bigint :customer_id
      t.string :money_bird_invoice_id
      t.text :details
      t.timestamps
    end
  end
end
