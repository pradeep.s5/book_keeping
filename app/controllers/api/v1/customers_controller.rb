class CustomersController < BaseController

  def create
    response = CustomerCreatorService.new.execute(permitted_params)
  end


  def permitted_params
    params.require(:customer).permit(:first_name, :last_name)
  end

end
