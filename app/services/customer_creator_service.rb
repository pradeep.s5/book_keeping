class CustomerCreatorService

  def initialize(params)
    @customer = Customer.new(params)
  end

  def execute
    success = true
    message = "Created successfully"
    Customer.transaction do
      @customer.save!
      save_money_bird_details
    rescue => e
      success = false
      message e.message
      raise ActiveRecord::Rollback
    end
    return { success: success, message: message, customer: @customer }    
  end


  private

  def save_money_bird_details
    contact = create_money_bird_contact
    contact = JSON.parse(contact.body)
    @customer.update(money_bird_contact_id: contact["id"])
  end

  def create_money_bird_contact
    RestClient.post("#{$money_bird_client}/contacts.json", money_bird_params, { authorization: "Bearer #{MoneyBirdConfig.find_by_record_for("Authorization").access_token}" }
    )
  end

  def money_bird_params
    data = {:contact=>{:company_name=>"Test B.V.", city: "Indore", country: "India", firstname: @customer.first_name, lastname: @customer.last_name }}
  end
end