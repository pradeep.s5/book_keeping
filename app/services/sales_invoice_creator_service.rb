class SalesInvoiceCreatorService

  def initialize(params)
    @customer = Customer.find(params[:customer_id])
  end

  def execute
    success = true
    message = "Created successfully"
    Invoice.transaction do
      
    save_money_bird_invoice
    create_customer_invoice

    rescue => e
      success = false
      message = e.message
      raise ActiveRecord::Rollback
    end
    return { success: success, message: message }
  end

  private

  def save_money_bird_invoice
    @invoice = create_money_bird_invoice
    @invoice = JSON.parse(@invoice.body)
  end

  def create_money_bird_invoice
    RestClient.post("#{$money_bird_client}/sales_invoices", money_bird_invoice_params, { authorization: "Bearer #{MoneyBirdConfig.find_by_record_for("Authorization").access_token}" }
    )
  end

  def money_bird_invoice_params
    # For now we are passing static values. According to requirement we will pass all parameters in Graphql request and use.

    data = {:sales_invoice=>{:reference=> "32000", contact_id: @customer.money_bird_contact_id, country: "India", details_attributes: [{description:"Rocking Chair", price:129.95}, {description:"Table", price:100}]}}
  end

  def create_customer_invoice
    @customer.invoices.create(money_bird_invoice_id: @invoice['id'], details: @invoice.as_json)
  end

end