class Mutations::CreateInvoice < Mutations::BaseMutation
  argument :customer_id, Bignum, required: true
  # We can pass more arguments for invoices here

  field :message, [String]


  def resolve(arguments)
    response = SalesInvoiceCreatorService.new(arguments).execute
    { success: response[:success], message: response[:message].split(',') }
    # customer = Customer.new(first_name: first_name, last_name: last_name, money_bird_contact_id: money_bird_contact_id)
  end
end
