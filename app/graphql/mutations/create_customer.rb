class Mutations::CreateCustomer < Mutations::BaseMutation
  argument :first_name, String, required: true
  argument :last_name, String, required: true

  field :customer, Types::CustomerType
  field :message, [String]

  def resolve(arguments)
    response = CustomerCreatorService.new(arguments).execute
    { success: response[:success], message: response[:message].split(','), customer: response[:customer] }
    # customer = Customer.new(first_name: first_name, last_name: last_name, money_bird_contact_id: money_bird_contact_id)
  end
end