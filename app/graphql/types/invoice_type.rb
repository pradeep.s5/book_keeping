# frozen_string_literal: true

module Types
  class InvoiceType < Types::BaseObject
    field :id, ID, null: false
    field :customer_id, Bignum
    field :money_bird_invoice_id, String
    field :customer, Types::CustomerType, null: false
    field :details, String
    field :created_at, GraphQL::Types::ISO8601DateTime, null: false
    field :updated_at, GraphQL::Types::ISO8601DateTime, null: false
  end
end


