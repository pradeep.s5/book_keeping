module Types
  class MutationType < Types::BaseObject
    field :customer, [Types::CustomerType]
    field :create_customer, mutation: Mutations::CreateCustomer
    field :create_invoice, mutation: Mutations::CreateInvoice
  end
end
