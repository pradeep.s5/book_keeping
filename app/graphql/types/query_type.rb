module Types
  class QueryType < Types::BaseObject
    # Add `node(id: ID!) and `nodes(ids: [ID!]!)`
    include GraphQL::Types::Relay::HasNodeField
    include GraphQL::Types::Relay::HasNodesField

    # Add root-level fields here.
    # They will be entry points for queries on your schema.

    field :customers, [Types::CustomerType]

    def customers
      Customer.all
    end

    # Get a specific user
    field :customer, Types::CustomerType, null: false do
      argument :id, ID, required: true
    end

    # Get a specific invoice
    field :invoice, Types::InvoiceType, null: false do
      argument :id, ID, required: true
    end

    def customer(id:)
      Customer.find(id)
    end

    def invoice(id:)
      Invoice.find(id)
    end

  end
end
